# -*- coding=utf-8 -*-

import logging
import telebot
from telebot import types
import configparser

# Configuring bot
config = configparser.ConfigParser()
config.read_file(open('config.ini'))

DEBUG = (config['DEFAULT']['debug'] == 'True')

bot = telebot.TeleBot(config['DEFAULT']['token'])

# set logger
logger = logging.getLogger("controllerbot")
loglevel = logging.DEBUG if DEBUG else logging.INFO
logging.basicConfig(level=loglevel)
telebot.logger.setLevel(loglevel)

MAIN_MENU = types.ReplyKeyboardMarkup(resize_keyboard=True)
MAIN_MENU.row('Создать пост', 'Управление постами')
MAIN_MENU.row('Каналы', 'Настройки')

greeting = """
    @ControllerBot поможет вам создавать отложенные посты в ваш телеграм канал.
    Для дополнительной помощи наберите комманду /help
"""

help = """
Доступные команды:
"""

commands = {
    'start': 'Начать использовать бота',
    'help': 'Дополнительная информация по командам'
}


@bot.message_handler(commands=['start'])
def command_start(m):
    bot.send_message(m.chat.id, greeting, reply_markup=MAIN_MENU, parse_mode='Markdown')


@bot.message_handler(commands=['help'])
def command_help(m):
    help_text = help
    for key in commands:
        help_text += "/" + key + ": "
        help_text += commands[key] + "\n"
    bot.send_message(m.chat.id, help_text)


@bot.message_handler(func=lambda message: message.text == 'Главное меню' and message.content_type == 'text')
def main_menu(message):
    bot.send_message(message.chat.id, "*Главное меню*", reply_markup=MAIN_MENU, parse_mode='Markdown')

@bot.message_handler(func=lambda message: message.text == 'Каналы' and message.content_type == 'text')
def channels(message):
    markup = types.InlineKeyboardMarkup()
    add_new_channel = types.InlineKeyboardButton(text="Добавить новый канал", callback_data="add_new_channel")
    markup.add(add_new_channel)
    bot.send_message(message.chat.id, """
    *Управление каналами*
    
    Добавьте новый канал или выберите уже подключенный, чтобы добавить или удалить администраторов, изменить временную зону или удалить канал.
    """, reply_markup=markup, parse_mode='Markdown')


@bot.message_handler(func=lambda message: message.text == 'Настройки' and message.content_type == 'text')
def settings(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.row('Выбор языка')
    markup.row('Часовой пояс')
    markup.row('Главное меню')
    bot.send_message(message.chat.id, "*Настройки*", reply_markup=markup, parse_mode='Markdown')

@bot.callback_query_handler(func=lambda call: call.data == 'add_new_channel')
def callbacks(call):
    markup = types.InlineKeyboardMarkup()
    back_btn = types.InlineKeyboardButton(text="« Назад", callback_data="back_add_new_channel")
    markup.add(back_btn)
    bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text="""
    Чтобы добавить канал, вы должны выполнить следующие два шага:

1. Добавить данного бота администратором вашего канала.
2. Затем перешлите мне любое сообщение из вашего канала (вы также можете отправить @username вашего канала или id).
    """, reply_markup=markup)

# default handler
@bot.message_handler(func=lambda message: True, content_types=['text'])
def command_default(m):
    bot.send_message(m.chat.id, "Я не понимаю \"" + m.text + "\"\nДля помощи наберите команду /help")


if __name__ == '__main__':
    bot.polling(none_stop=True)
